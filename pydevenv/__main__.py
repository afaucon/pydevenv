import argparse
import os
import sys

from .__info__ import __package_name__, __description__, __version__
from .api import get_all, find_from_keyword
from .display import pretty_print_all_as_table


def pydevenv_cli():
    parser = argparse.ArgumentParser(prog=__package_name__, 
                                     description=__description__)
    parser.add_argument('--version', action='version', version=__version__, help='Display the version')

    subparsers = parser.add_subparsers(dest="command")

    # create the parser for the "display" command
    _ = subparsers.add_parser('display', help='Display information to the user about all available development environments')

    # create the parser for the "find_from_keyword" command
    parser_find_from_keyword = subparsers.add_parser('find_from_keyword', help='Get the path of a development environment based on its name')
    parser_find_from_keyword.add_argument('name', nargs='?', help='Name of the development environment')

    # create the parser for the "list" command
    parser_list = subparsers.add_parser('list', help='List paths of all available development environments')
    parser_list.add_argument('--name-only', action='store_true', help='Folder names only')

    # parse
    args = parser.parse_args()

    root_paths = os.getenv('DEV_PATH').split(':')

    if args.command is None:
        args.command = 'display'

    if args.command == 'display':
        all_paths = get_all(root_paths)
        pretty_print_all_as_table(all_paths, root_paths)
        sys.exit(0)
    elif args.command == 'find_from_keyword':
        candidates = find_from_keyword(root_paths, args.name)
        if len(candidates) == 0:
            sys.exit(1)
        elif len(candidates) == 1:
            print(candidates[0])
            sys.exit(0)
        else:
            for candidate in candidates:
                print(candidate)
            sys.exit(0)
    elif args.command == 'list':
        for path in get_all(root_paths, args.name_only):
            print(path)
        sys.exit(0)

if __name__ == "__main__":
    pydevenv_cli()