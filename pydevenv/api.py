import os

import pydevenv.exceptions


def __name_only(paths_list):
    return [ item.split('/')[-1] for item in paths_list ]

def get_all(root_paths, name_only=False):
    paths_list = []
    for root_path in root_paths:
        new_paths_list = []
        for root, dirs, files in os.walk(root_path, topdown=True):
            if '.git' in dirs or '.svn' in dirs:
                new_paths_list.append(root)
                dirs[:] = []  # Allows to stop the topdown recursion, to do the job faster
        paths_list += new_paths_list
    if name_only:
        return __name_only(paths_list)
    return paths_list

def find_from_keyword(root_paths, keyword, name_only=False):
    # First: Test the equality
    # Second: If not exact match, test if keyword is contained in a foldername
    all_dev_env_paths = get_all(root_paths)
    matches = [ path for path in all_dev_env_paths if keyword.lower() == path.split('/')[-1] ]
    if len(matches) == 0:
        matches = [ path for path in all_dev_env_paths if keyword.lower() in path.split('/')[-1] ]
    if name_only:
        return __name_only(matches)
    return matches

def new_servcie():
    raise pydevenv.exceptions.NotYetImplementedException
