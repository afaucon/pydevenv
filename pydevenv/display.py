import prettytable

import pydevenv.utils


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def get_remote_category_for_grouping(env_object):
    remote_url = env_object.get_remote_url()
    remote_url_splitted = remote_url.split('/')
    if isinstance(env_object, pydevenv.utils.GitEnv):
        if 'github' in remote_url or 'gitlab' in remote_url:
            return '/'.join(remote_url_splitted[0:2])
        else:
            return remote_url_splitted[0]
    elif isinstance(env_object, pydevenv.utils.SvnEnv):
        return '/'.join(remote_url_splitted[0:3])

def pretty_print_all_as_table(all_paths, root_paths):
    from pathlib import Path
    all_paths_by_root = { root_path : [] for root_path in root_paths }
    for path in all_paths:
        path = Path(path)
        for root_path in root_paths:
            root_path = Path(root_path)
            if path.is_relative_to(root_path):
                all_paths_by_root[str(root_path)].append(str(path))

    for root_path, all_paths in all_paths_by_root.items():
        print('# ' + root_path)
        print()
        env_objects = [ pydevenv.utils.make_env_object(env_path) for env_path in all_paths ]
        env_objects.sort(key=lambda x: x.get_dirname().lower())
        env_objects.sort(key=lambda x: get_remote_category_for_grouping(x))
        
        pretty_table = prettytable.PrettyTable(align='l')
        pretty_table.field_names = ["Name", "Remote", "Status", "Current branch", "# branches"]
        previous_remote_category_for_grouping = None
        for env_object in env_objects:
            if previous_remote_category_for_grouping is not None:
                if previous_remote_category_for_grouping != get_remote_category_for_grouping(env_object):
                    pretty_table.add_row(['', '', '', '', '']) # Add spacing

            name = env_object.get_dirname()
            remote_url = env_object.get_remote_url()
            status = env_object.get_status()
            if status == 'Clean':
                status = bcolors.OKGREEN + status + bcolors.ENDC
            elif status == 'Untracked':
                status = bcolors.WARNING + status + bcolors.ENDC
            elif status == 'Modified':
                status = bcolors.FAIL + status + bcolors.ENDC
            if isinstance(env_object, pydevenv.utils.GitEnv):
                current_branch = env_object.get_current_branch()
                nlocal_without_remote, nlocal, nremote = env_object.get_number_of_branches()
                if nlocal_without_remote > 0:
                    number_of_branches = bcolors.FAIL + str(nlocal_without_remote) + bcolors.ENDC + '/' + str(nlocal) + '/' + str(nremote)
                else: 
                    number_of_branches = str(nlocal_without_remote) + '/' + str(nlocal) + '/' + str(nremote)
            else:
                current_branch = ''
                number_of_branches = ''
            pretty_table.add_row([name, remote_url, status, current_branch, number_of_branches])

            # Grouping remote
            previous_remote_category_for_grouping = get_remote_category_for_grouping(env_object)
        print(pretty_table)
        print()