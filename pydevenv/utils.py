import os
import re
import subprocess


class GitEnv:
    def __init__(self, path):
        self.path = path

    def get_dirname(self):
        return self.path.split('/')[-1]
        
    def get_remote_url(self):
        result = subprocess.run(['git', '-C', self.path, 'remote', '-v'], capture_output=True, text=True).stdout.strip()
        if result == '':
            return ''
        result_lines = result.split('\n')
        if len(result_lines) <= 2:
            # One remote
            for line in result_lines:
                remote_url = line.split('\t')[1].split(' ')[0]
                if '(fetch)' in line.split('\t')[1].split(' ')[1]:
                    remote_fetch_url = remote_url
                if '(push)' in line.split('\t')[1].split(' ')[1]:
                    remote_push_url = remote_url
            if remote_fetch_url == remote_push_url:
                # remote url postprocessing
                if remote_fetch_url.startswith('git@'):
                    remote_fetch_url = remote_fetch_url[4:].replace(':', '/')
                if remote_fetch_url.startswith('https://'):
                    remote_fetch_url = remote_fetch_url[8:]
                if remote_fetch_url.startswith('http://'):
                    remote_fetch_url = remote_fetch_url[7:]
                return remote_fetch_url
            else:
                return 'The remote has different push and fetch urls'
        else:
            # Several remotes
            return 'Several remotes defined'
    
    def get_status(self):
        result = subprocess.run(['git', '-C', self.path, 'status', '--porcelain'], capture_output=True, text=True).stdout.strip()
        if result == '':
            return 'Clean'
        result_lines = result.split('\n')
        untracked = 0
        modified = 0
        for line in result_lines:
            status_code = line.split(' ')[0]
            if status_code == '??':
                untracked += 1
            else:
                modified += 1
        if modified > 0:
            return 'Modified'
        else:
            return 'Untracked'
    
    def get_current_branch(self):
        return subprocess.run(['git', '-C', self.path, 'rev-parse', '--abbrev-ref', 'HEAD'], capture_output=True, text=True).stdout.strip()
    
    def get_number_of_branches(self):
        result = subprocess.run(['git', '-C', self.path, 'branch', '-avv'], capture_output=True, text=True).stdout.strip()
        if result == '':
            return 0, 0, 0
        result_lines = result.split('\n')
        number_of_local_without_remote_tracking = 0
        number_of_local = 0
        number_of_remote = 0
        for line in result_lines:
            if line.startswith('*'):
                line = line[1:]
            line = line.strip()
            if not line.startswith('remotes/'):
                number_of_local += 1
                result = re.search('\[(.*)\]', line)
                if result is None:
                    number_of_local_without_remote_tracking += 1
            else:
                if not line.split(' ')[0].endswith('/HEAD'):
                    number_of_remote += 1
        return number_of_local_without_remote_tracking, number_of_local, number_of_remote

class SvnEnv:
    def __init__(self, path):
        self.path = path

    def get_dirname(self):
        return self.path.split('/')[-1]
        
    def get_remote_url(self):
        remote_url = subprocess.run(['svn', 'info', self.path, '--show-item=url'], capture_output=True, text=True).stdout.strip()
        if remote_url == '':
            return ''
        else:
            if remote_url.startswith('https://'):
                remote_url = remote_url[8:]
            if remote_url.startswith('http://'):
                remote_url = remote_url[7:]
            return remote_url

    def get_status(self):
        result = subprocess.run(['svn', 'status', self.path], capture_output=True, text=True).stdout.strip()
        if result == '':
            return 'Clean'
        result_lines = result.split('\n')
        untracked = 0
        modified = 0
        for line in result_lines:
            status_code = line.split(' ')[0]
            if status_code == '?':
                untracked += 1
            else:
                modified += 1
        if modified > 0:
            return 'Modified'
        else:
            return 'Untracked'

def is_git_repo(path):
    return '.git' in os.listdir(path)

def is_svn_repo(path):
    return '.svn' in os.listdir(path)

def make_env_object(path):
    if is_git_repo(path):
        return GitEnv(path)
    if is_svn_repo(path):
        return SvnEnv(path)