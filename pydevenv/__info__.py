__package_name__ = "pydevenv"
__description__ = 'Get development environements that are locally available'
__url__ = 'https://gitlab.com/afaucon/pydevenv'
__version__ = '1.0-dev'
__author__ = 'Adrien Faucon'
__author_email__ = 'adrien.faucon@gmail.com'
__license__ = 'MIT License'
