# Lists and selects development environment

## Installation

### For users

Install the package [from GitLab](https://pip.pypa.io/en/stable/reference/pip_install/#git).

```bash
$ pip install git+https://github.com/afaucon/pydevenv.git
$ pip list
```

### For developpers

Clone the package from GitHub and install it in editable mode (i.e. [setuptools "develop mode"](https://setuptools.readthedocs.io/en/latest/setuptools.html#development-mode)).

```bash
$ git clone git+https://github.com/afaucon/pydevenv.git
$ pip install --editable pydevenv
$ pip list
```

## Usage

### Prerequisites

First, set `DEV_PATH` environment variable. This variable stores one or several paths to a directory that stores multiple repositories folders.
The same syntax as the `PATH` environment variable is used.

### Examples

Within a python module:

```python
import pydevenv

pydevenv.__version__
```

```python
import pydevenv

environments_paths = pydevenv.get_all_paths()
```

With the command line interface:

```bash
$ python -m pydevenv --version
1.0-dev
```

Or directly:

```bash
$ pydevenv --help
usage: pydevenv [-h] [--version] {display,get_path_by_name,list} ...

Get development environements that are locally available

positional arguments:
  {display,get_path_by_name,list}
    display             Display information to the user about all available development environments
    get_path_by_name    Get the path of a development environment based on its name
    list                List paths of all available develoment environments

options:
  -h, --help            show this help message and exit
  --version             Display the version
```
